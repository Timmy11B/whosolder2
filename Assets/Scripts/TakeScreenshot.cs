﻿using UnityEngine;
using System.Collections;

public class TakeScreenshot : MonoBehaviour {

    public bool LandscapeScreenshots = false;
    int currentscreenNumber = 0;
    void Start() {
        CreateAllDirectories();
    }


    void Update() {

        if (Input.GetKeyDown(KeyCode.K)) {
            StartCoroutine(takeAllScreenShots());
        } }

    void createDir(string dir) {
        if (!System.IO.Directory.Exists(dir))
            System.IO.Directory.CreateDirectory(dir);
    }

    void CreateAllDirectories() {
        
        createDir("listing");
        createDir("listing/3.5");   
        createDir("listing/4"); 
        createDir("listing/4.7");  
        createDir("listing/5.5");  
        createDir("listing/ipad");
        createDir("listing/ipadPRO");
        while (System.IO.File.Exists("listing/3.5/" + currentscreenNumber + ".png")) {
            currentscreenNumber++;
        }


    }


    IEnumerator takeAllScreenShots()
    {
        float originalTimescale = Time.timeScale;
        Time.timeScale = 0;
      
        yield return TakeHQScreenshot("listing/3.5/", 640, 960);
        yield return TakeHQScreenshot("listing/4/", 640, 1096);
        yield return TakeHQScreenshot("listing/4.7/", 750, 1334);
        yield return TakeHQScreenshot("listing/5.5/", 1242, 2208);
        yield return TakeHQScreenshot("listing/ipad/", 768, 1004);
        yield return TakeHQScreenshot("listing/ipadPRO/", 2048, 2732);
        currentscreenNumber++;
        Time.timeScale = originalTimescale;
    }





    IEnumerator TakeHQScreenshot(string path,int resWidth,int resHeight) {

        if (LandscapeScreenshots) {
            int tempval = resHeight;
            resHeight = resWidth;
            resWidth = tempval;
        }

        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        Camera.main.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        Camera.main.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        Camera.main.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = path+currentscreenNumber+".png";
        System.IO.File.WriteAllBytes(filename, bytes);

        
        yield return null;
    }



}
