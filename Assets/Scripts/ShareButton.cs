﻿using UnityEngine;
using System.Collections;

public class ShareButton : MonoBehaviour {
    public string ClickableURL = "https://twitter.com/chupamobile";


    public void GoToUrl() {
        Application.OpenURL(ClickableURL);
    }

}
