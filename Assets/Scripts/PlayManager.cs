﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayManager : MonoBehaviour {

    public Text txt_name1_top, txt_value1_top, txt_name2_bot, txt_value2_bot, txt_name3_under;
    public Button topButton, BottomButton;
    int searchIndexTOP;
    int searchIndexBOT;
    int listLenght;
    public static PlayManager instance;
    float SecondsToContinueGame = 2;
    float SecondsToEndGame = 3;

    public GameObject gameplayUI, gameoverUI;

    float AnswerShowPosition;

    void Awake() {
        instance = this;
    }

    void Start() {
        Application.targetFrameRate = 60;
        AnswerShowPosition = txt_value2_bot.transform.position.y;
        txt_value2_bot.gameObject.SetActive(false);

        listLenght = List.instance.itemList.Length;

        NewMatch(true);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
            NewMatch(true);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            NewMatch();
        }
    }



    public void NewMatch(bool reset = false) {

        ShowUI(false);

        ShowTheQuestion();
        if (listLenght < 2){
        Debug.LogError("THE \"LIST OF QUERIES\" CONTAINS LESS THAN 2 SCORES! Are you mad bro?");        
        }   

        else
        {
            if (reset)
            {

                ScoreHandler.instance.reset();
                ScoreHandler.instance.incrementNumberOfGames();
                if (OneMoreChance.instance!=null)
                OneMoreChance.instance.EnableOneMoreChance();
            }

            GetNewIndexes(reset);
            CreateQuestion();
        }

       


    }

    void ShowUI(bool gameOver) {

            gameplayUI.SetActive(!gameOver);
            gameoverUI.SetActive(gameOver);
       

    }

    public void AnswerQuestion(bool answer) {
        bool correctAnswer = false;

    

        if (List.instance.itemList[searchIndexTOP].Searches <= List.instance.itemList[searchIndexBOT].Searches && answer == true) {

            correctAnswer = true;
        }
        if (List.instance.itemList[searchIndexTOP].Searches >= List.instance.itemList[searchIndexBOT].Searches && answer == false)
        {
            correctAnswer = true;
        }


        ShowTheAnswer();
        if (correctAnswer) { MatchWin(); } else { MatchLoose(); }

    }


    void MatchWin() {
        SymbolEffect.instance.ActivateEffect(SymbolEffect.Effect.correct);
        StartCoroutine(dealayedContinue());
        ScoreHandler.instance.increaseScore(1);

    }
    void MatchLoose() {
        SymbolEffect.instance.ActivateEffect(SymbolEffect.Effect.wrong);
        StartCoroutine(dealayedContinue(false));
        AdNetworks.instance.ShowInterstitial();
       
        Debug.Log("GAME OVER: GOING TO GAMEOVER MENU: Interstitial here");

    }

    IEnumerator dealayedContinue(bool continueGame = true)
    {
        float effect = 0;
        float effectTime = SecondsToContinueGame;
        if (continueGame == false) effectTime = SecondsToEndGame;

        while (effect <= effectTime)
        {
            effect += Time.deltaTime;           
            yield return new WaitForEndOfFrame();
        }
        if (continueGame)
        {
            NewMatch();
        }
        else {
            ShowUI(true);

        }

    }


    void ShowTheAnswer() {
        txt_value2_bot.gameObject.SetActive(true);
        topButton.gameObject.SetActive(false);
        BottomButton.gameObject.SetActive(false);
        StartCoroutine(TextAnswerEffect());
    }

    void ShowTheQuestion()
    {
        txt_value2_bot.gameObject.SetActive(false);
        topButton.gameObject.SetActive(true);
        BottomButton.gameObject.SetActive(true);
    }


    IEnumerator TextAnswerEffect() {
        Vector3 answerPosition = txt_value2_bot.transform.position;
        answerPosition.y = AnswerShowPosition;
        txt_value2_bot.transform.position = answerPosition;
        Vector3 hiddenAnswerPos = answerPosition;
        hiddenAnswerPos.y -= 1.5f;
        txt_value2_bot.transform.position = hiddenAnswerPos;
        float lerper = 0;
        float lerperTime = 0.35f;
        while (lerper <= 1) {
            lerper += Time.deltaTime / lerperTime;
            txt_value2_bot.transform.position = Vector3.Lerp(hiddenAnswerPos, answerPosition, lerper);
            yield return new WaitForEndOfFrame();
        }
    }


    void CreateQuestion() {
        txt_name1_top.text = "\"" + List.instance.itemList[searchIndexTOP].Name + "\"";
        txt_value1_top.text = System.String.Format("{0:n0}", List.instance.itemList[searchIndexTOP].Searches)  +"" ;

        txt_name2_bot.text = "\"" + List.instance.itemList[searchIndexBOT].Name + "\"";
        // txt_value2_bot.text = System.String.Format("{0:n0}", List.instance.itemList[searchIndexBOT].Searches) +"";

        int year = System.DateTime.Now.Year;
        txt_value2_bot.text = year- List.instance.itemList[searchIndexBOT].Searches + " years old";

         txt_name3_under.text = "searches than " + List.instance.itemList[searchIndexTOP].Name + "";
        SymbolEffect.instance.ActivateEffect(SymbolEffect.Effect.versus);
        SetQuestionImages();
    }


    public RawImage topImage, bottomImage;
    void SetQuestionImages() {
        topImage.texture = List.instance.itemList[searchIndexTOP].imag;
        bottomImage.texture = List.instance.itemList[searchIndexBOT].imag;

    }



    void GetNewIndexes(bool startAgain = false) {

        if (startAgain == true)
        {
            searchIndexTOP = Random.Range(List.instance.startingIndex, listLenght);
        }
        else {
            searchIndexTOP = searchIndexBOT;
        }
        

        searchIndexBOT = Random.Range(List.instance.startingIndex, listLenght);

        // recycle because maybe you get the same value
        while (searchIndexBOT == searchIndexTOP)
        {
            searchIndexBOT = Random.Range(List.instance.startingIndex, listLenght);
        }
    }


    

}
