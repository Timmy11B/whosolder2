﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SymbolEffect : MonoBehaviour {

   public Sprite Versus, Correct, Wrong;
     Image symbolImage;
    public enum Effect {
        versus,correct,wrong,hide
    }


    public static SymbolEffect instance;
    void Awake() {
        symbolImage = GetComponent<Image>();
        instance = this;
         }


    public void ActivateEffect(Effect effect) {
        symbolImage.enabled = true;

        if (effect == Effect.correct) {
            symbolImage.sprite = Correct;
        }
        if (effect == Effect.wrong)
        {
            symbolImage.sprite = Wrong;
        }
        if (effect == Effect.versus)
        {
            symbolImage.sprite = Versus;
        }
        if (effect == Effect.hide)
        {
            symbolImage.enabled = false ;
        }

        StartCoroutine(EnlargeCoroutine());
    }

    IEnumerator EnlargeCoroutine() {
        Vector3 originalscale = new Vector3(1,1,1);
        symbolImage.transform.localScale = Vector3.zero;
        float lerper = 0;
        float lerperTime = 0.35f;

        while (lerper <= 1) {
            lerper += Time.deltaTime / lerperTime;
            symbolImage.transform.localScale = Vector3.Lerp(Vector3.zero, originalscale, lerper);
                yield return new WaitForEndOfFrame();
        }
        
    }



}
