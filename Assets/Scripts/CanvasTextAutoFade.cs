﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CanvasTextAutoFade : MonoBehaviour {
    bool autofadeAtStart = true;
    Text[] allCanvasText;
    string[] allTextStrings;
	// Use this for initialization
	void Start () {
        allCanvasText = GetComponentsInChildren<Text>();
        GetAllTextStrings();
	}


    void GetAllTextStrings() {
        allTextStrings = new string[allCanvasText.Length];
        if (autofadeAtStart == false)
        {
            for (int i = 0; i < allCanvasText.Length; i++)
            {
                allTextStrings[i] = allCanvasText[i].text;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {    
            for (int i = 0; i < allCanvasText.Length; i++) {
                if (allTextStrings[i] != allCanvasText[i].text) {
                    allTextStrings[i] = allCanvasText[i].text;
                StartCoroutine(textFadeEffect(allCanvasText[i]));
                }
        }
            
    }


    IEnumerator textFadeEffect(Text target) {

        float lerper = 0;
        float lerperTime = 0.55f;
        Color original = target.color;
        original.a = 1;
        Color invisible = original;
        invisible.a = 0;
        while (lerper <= 1) {
            lerper += Time.deltaTime / lerperTime;
            target.color = Color.Lerp(invisible, original, lerper);
            yield return new WaitForEndOfFrame();
        }
    }


}
